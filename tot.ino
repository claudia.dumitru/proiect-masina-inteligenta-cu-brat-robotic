#include <Servo.h>

int claw_open = 90;
int claw_closed = 170;

int arm_up = 10;
int arm_down = 135;

int base_center = 65;
int base_right = 0;
int base_left = 160;

Servo myservo1;//servo of claw
Servo myservo2;//servo of arm
Servo myservo3;//servo of base

int Left_Tra_Value = 1;
int Center_Tra_Value = 1;
int Right_Tra_Value = 1;
int Black_Line = 1;

void setup(){
  Serial.begin(9600);
  
  //roti
  pinMode(2, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(8, INPUT);
  pinMode(7, INPUT);
  pinMode(A1, INPUT);

  //Distanta
  pinMode(12, OUTPUT);
  pinMode(13, INPUT);

  //mana
  myservo1.attach(9);
  myservo2.attach(12);
  myservo3.attach(11);
  myservo1.write(claw_closed);
  myservo2.write(arm_up);
  myservo3.write(base_center);
}

void loop()
{
 move_to_obstacle();
}

void move_to_obstacle()
{
  if (checkdistance() > 20)
  {
    trackLine();
  }
  else
  {
    Stop();
    grabObstacle();
  }
}

void trackLine()
{
  Left_Tra_Value = digitalRead(7);
  Center_Tra_Value = digitalRead(8);
  Right_Tra_Value = digitalRead(A1);
  
  if (Left_Tra_Value != Black_Line && Center_Tra_Value == Black_Line && Right_Tra_Value != Black_Line) {
   Move_Forward(90);

  } else if (Left_Tra_Value == Black_Line && Center_Tra_Value == Black_Line && Right_Tra_Value != Black_Line) {
    Rotate_Left(60);
    
  } else if (Left_Tra_Value == Black_Line && Center_Tra_Value != Black_Line && Right_Tra_Value != Black_Line) {
    Rotate_Left(100);
    
  } else if (Left_Tra_Value != Black_Line && Center_Tra_Value != Black_Line && Right_Tra_Value == Black_Line) {
    Rotate_Right(100);
    
  } else if (Left_Tra_Value != Black_Line && Center_Tra_Value == Black_Line && Right_Tra_Value == Black_Line) {
    Rotate_Right(60);
    
  } else if (Left_Tra_Value == Black_Line && Center_Tra_Value == Black_Line && Right_Tra_Value == Black_Line) {
    Stop();
  }
  else if (Left_Tra_Value != Black_Line && Center_Tra_Value != Black_Line && Right_Tra_Value != Black_Line){
    Move_Backward(60);
  }
}


float checkdistance() {
  digitalWrite(12, HIGH);
  delayMicroseconds(10);
  digitalWrite(12, LOW);
  float distance = pulseIn(13, HIGH) / 58.00;
  delay(45);
  Serial.println(distance);
  return distance;
}

void grabObstacle() 
{
  myservo3.write(base_center);
  delay(1000);
  myservo1.write(claw_open);
  delay(1000);
  myservo2.write(arm_down);
  delay(1000);
  Move_Forward(80);
  delay(400);
  Stop();
  myservo1.write(claw_closed);
  delay(1000);
  myservo2.write(arm_up);
  delay(1000);
  myservo3.write(base_right);
  delay(1000);
  myservo2.write(arm_down);
  delay(1000);
  myservo1.write(claw_open);
  delay(1000);
  myservo2.write(arm_up);
  delay(1000);
  myservo3.write(base_center);
  delay(1000);
  myservo1.write(claw_closed);
  delay(1000);
}

void Move_Forward(int speed) 
{
  digitalWrite(2,HIGH); 
  analogWrite(5,speed); 
  digitalWrite(4,LOW);
  analogWrite(6,speed);
}


void Stop() 
{
  digitalWrite(2,LOW);
  analogWrite(5,0);
  digitalWrite(4,HIGH);
  analogWrite(6,0);
}

void Rotate_Right(int speed) {
  digitalWrite(2,HIGH);
  analogWrite(5,speed+35);
  digitalWrite(4,HIGH);
  analogWrite(6,speed+25);
}

void Rotate_Left(int speed) {
  digitalWrite(2,LOW);
  analogWrite(5,speed+35);
  digitalWrite(4,LOW);
  analogWrite(6,speed+25);
}

void Move_Backward(int speed)
{
  digitalWrite(2,LOW);
  analogWrite(5,speed);
  digitalWrite(4,HIGH);
  analogWrite(6,speed);
}
